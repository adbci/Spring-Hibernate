-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 23 Mars 2017 à 17:06
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `fenetre`
--

--
-- Contenu de la table `civilite`
--

INSERT INTO `civilite` (`id`, `libelle`) VALUES
(1, 'Monsieur'),
(2, 'Madame'),
(3, 'Mademoiselle');

--
-- Contenu de la table `couleur`
--

INSERT INTO `couleur` (`id`, `codeRgb`, `nom`) VALUES
(1, '#ffffff', 'Blanc'),
(2, '#666666', 'Anthracite'),
(3, '#122b40', 'Bleu');

--
-- Contenu de la table `demande`
--

INSERT INTO `demande` (`id`, `dateDemande`, `dateReponse`, `hauteurEnCm`, `informationComplementaire`, `largeurEnCm`, `prix`, `couleur_id`, `materiau_id`, `personne_id`, `vitrage_id`) VALUES
(7, '2017-03-23 14:53:52', NULL, 120, 'Avez vous d''autre couleurs svp?', 80, NULL, 1, 1, 4, 1),
(8, '2017-03-23 14:56:42', NULL, 1200, 'C''est cher?', 80, NULL, 2, 3, 1, 3),
(9, '2017-03-23 15:33:17', NULL, 120, 'RAS', 80, NULL, 3, 1, 1, 1),
(10, '2017-03-23 15:33:36', NULL, 120, 'Cool, mais faites vite svp...!', 80, NULL, 1, 3, 1, 2),
(11, '2017-03-23 15:33:49', NULL, 120, 'RAS non plus', 80, NULL, 1, 2, 1, 3),
(12, '2017-03-23 15:35:45', NULL, 120, 'Belles fenetres!', 80, NULL, 3, 2, 1, 3),
(13, '2017-03-23 15:48:01', NULL, 120, 'RAS', 80, NULL, 1, 1, 5, 1),
(14, '2017-03-23 16:01:14', NULL, 120, 'Je veux une remise ', 80, NULL, 1, 1, 6, 1);

--
-- Contenu de la table `materiau`
--

INSERT INTO `materiau` (`id`, `nom`) VALUES
(1, 'PVC'),
(2, 'Bois'),
(3, 'Aluminium');

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`DTYPE`, `id`, `email`, `motDePasse`, `nom`, `prenom`, `dateDeNaissance`, `dateEmbauche`, `civilite_id`) VALUES
('Client', 1, 'adrien@gmail.com', '000000', 'Bucci', 'Adrien', '1983-01-07 00:00:00', NULL, 1),
('Client', 2, 'email', '000000', 'Bucci', 'Adrien', '1983-01-07 00:00:00', NULL, 1),
('Client', 3, 'adrien@gmail.com', '000000', 'Bucci', 'Adrien', '1983-01-07 00:00:00', NULL, 1),
('Client', 4, 'lou@gmail.com', '000000', 'Belin', 'Lou', '1988-08-10 00:00:00', NULL, 3),
('Client', 5, 'jean@gmail.com', '000000', 'Bucci', 'Jean', '1983-01-07 00:00:00', NULL, 1),
('Client', 6, 'michel@gmail.com', '000000', 'Tatz', 'Michel', '1978-01-07 00:00:00', NULL, 1),
('Expert', 7, 'yannick@gmail.com', '000000', 'Laforce', 'Yannick', '1980-03-23 16:03:51', '2017-03-23 16:03:44', 1),
('Expert', 8, 'christophe@gmail.com', '000000', 'Couette', 'Christophe', '1987-03-23 16:04:37', '2004-07-28 16:04:44', 1);

--
-- Contenu de la table `vitrage`
--

INSERT INTO `vitrage` (`id`, `description`, `nom`) VALUES
(1, 'Epaisseur 4-16-4', 'Double 4-16-4'),
(2, 'Epaisseur 4-20-4', 'Double 4-20-4'),
(3, 'Epaisseur 4-8-4-8-4', 'Triple 4-8-4-8-4');
