<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Connecion r�ussie!</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
</head>
<body>


<div class="container-fluid">

    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center pt-20">

            <h1 class="form-signin-heading">K par K</h1>
            <h2 class="form-signin-heading">Espace clients</h2>

            <div class="btn-success">
                Connection r�ussie!
            </div>
            <div class="pt-20">
                Merci <c:out value="${client.prenom}"/> de vous �tre connect�!
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-md-offset-4 text-center pt-20">

        Vous pouvez consulter <a href="/account">votre compte</a> <br/>
        ou <a href="/demandePrix">effectuer une demande de prix</a>
        <br/>
        <br/>

    </div>
</div>
</body>
</html>