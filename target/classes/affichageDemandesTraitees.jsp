<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib uri="http://example.com/functions" prefix="f" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>App datas</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
</head>
<body>

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12 text-center pt-20">

            <h1 class="form-signin-heading">K par K</h1>
            <h2 class="form-signin-heading">Espace d'administration experts</h2>

            <c:out value="${nbTotal}"/> demandes -

            Page en cours <c:out value="${pageEnCours}"/>
            /<c:out value="${nbPages}"/>

            <br/><br/>

            <table class="table table-bordered">
                <thead>
                <td colspan="7">
                    <c:forEach items="${navigation}" var="navigation">
                        <c:choose>
                            <c:when test="${navigation eq pageEnCours}">
                                ${navigation} &nbsp;
                            </c:when>
                            <c:otherwise>
                                <a href="afficherDemandes?page=${navigation}&sort=${sort}&sens=${sens}">${navigation}</a> &nbsp;
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </td>
                </thead>
                <tr>
                    <td>
                        Date/ heure demande
                    </td>
                    <td>
                        Demandeur <a href="afficherDemandes?page=${pageEnCours}&sort=personne_id&sens=asc"><img
                            src="/resources/asc.gif" title="ASC"/></a>
                        <a href="afficherDemandes?page=${pageEnCours}&sort=personne_id&sens=desc"><img
                                src="/resources/desc.gif" title="DESC"/></a>
                    </td>
                    <td>
                        Mat�riaux <a href="afficherDemandes?page=${pageEnCours}&sort=materiau_id&sens=asc"><img
                            src="/resources/asc.gif" title="ASC"/></a>
                        <a href="afficherDemandes?page=${pageEnCours}&sort=materiau_id&sens=desc"><img
                                src="/resources/desc.gif" title="DESC"/></a>
                    </td>
                    <td>
                        Vitrage <a href="afficherDemandes?page=${pageEnCours}&sort=vitrage_id&sens=asc"><img
                            src="/resources/asc.gif" title="ASC"/></a>
                        <a href="afficherDemandes?page=${pageEnCours}&sort=vitrage_id&sens=desc"><img
                                src="/resources/desc.gif" title="DESC"/></a>
                    </td>
                    <td>
                        Couleur <a href="afficherDemandes?page=${pageEnCours}&sort=couleur&sens=asc"><img
                            src="/resources/asc.gif" title="ASC"/></a>
                        <a href="afficherDemandes?page=${pageEnCours}&sort=couleur&sens=desc"><img
                                src="/resources/desc.gif" title="DESC"/></a>
                    </td>
                    <td>
                        Proposition
                    </td>
                    <td>
                        Temps reponse
                    </td>
                </tr>
                <c:forEach items="${demandesTraiteesToDisplay}" var="demandesTraiteesToDisplay">
                    <tr>
                        <td>
                                ${demandesTraiteesToDisplay.dateDemande}

                        </td>
                        <td>
                                ${demandesTraiteesToDisplay.personne.civilite}
                                ${demandesTraiteesToDisplay.personne.nom}
                                ${demandesTraiteesToDisplay.personne.prenom}
                        </td>
                        <td>
                                ${demandesTraiteesToDisplay.materiau}
                        </td>
                        <td>
                                ${demandesTraiteesToDisplay.vitrage}
                        </td>
                        <td class="bg-color-${demandesTraiteesToDisplay.couleur}">

                                ${demandesTraiteesToDisplay.couleur}
                        </td>
                        <td>
                                ${demandesTraiteesToDisplay.prix} &euro;

                        </td>
                        <td>

                                ${f:displayDateDiff(demandesTraiteesToDisplay.dateDemande, demandesTraiteesToDisplay.dateReponse)}

                        </td>
                    </tr>
                </c:forEach>
            </table>

            <br/>
            <a href="/afficherDemandes?page=1&sort=id&sens=asc" class="btn btn-info">Retour devis</a>
            <a href="/account" class="btn btn-info">Compte</a>
            <a href="/logout" class="btn btn-info">Logout</a>
        </div>
    </div>

</div>
</body>
</html>