<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>K par K - Inscription client</title>
    <script>
        function chargerText() {
            document.getElementById('nom').value = "Bucci";
            document.getElementById('prenom').value = "Adrien";
            document.getElementById('email').value = "adrien@gmail.com";
            document.getElementById('motDePasse').value = "000000";
            document.getElementById('dateDeNaissance').value = "07/01/1983";
            document.getElementById('civilite').value = "1";
        }
    </script>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
</head>
<body>

<div class="container-fluid">
    <form:form modelAttribute="inscriptionForm" method="post"
               cssClass="form-signin" action="/inscription">

        <div class="row pt-20">
            <div class="col-md-4 col-md-offset-4 text-center">

                <h1 class="form-signin-heading">K par K</h1>
                <h2 class="form-signin-heading">Inscrivez vous</h2>
                <h6 class="form-signin-heading">Tous les champs sont obligatoires</h6>

            </div>
            <div class="col-md-4 col-md-offset-2 pt-20">

                <form:input path="nom" placeHolder="Nom" cssClass="form-control"/>
                <div class="btn-warning">
                    <form:errors path="nom"/>
                </div>
                <br/>

                <form:input path="prenom" placeHolder="Pr�nom" cssClass="form-control"/>
                <div class="btn-warning">
                    <form:errors path="prenom"/>
                </div>
                <br/>

                <form:input path="dateDeNaissance" placeHolder="dd/MM/yyyy" cssClass="form-control"/>
                <div class="btn-warning">
                    <form:errors path="dateDeNaissance"/>
                </div>
                <br/>
            </div>

            <div class="col-md-4 pt-20">

                <form:input path="email" placeHolder="Email" cssClass="form-control"/>
                <div class="btn-warning">
                    <form:errors path="email"/>
                </div>
                <br/>

                <form:password path="motDePasse" placeHolder="Mot de passe" cssClass="form-control"/>
                <div class="btn-warning">
                    <form:errors path="motDePasse"/>
                </div>
                <br/>

                <form:select path="civilite" cssClass="form-control">
                    <form:option value="0">Veuillez s�l�ctionner une civilit�</form:option>
                    <c:forEach var="civilite" items="${civilites}">
                        <form:option value="${civilite.id}">${civilite.libelle}</form:option>
                    </c:forEach>
                </form:select>
                <div class="btn-warning">
                    <form:errors path="civilite"/>
                </div>
                <br/>

            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center pt-20">
                <form:button cssClass="btn btn-danger"
                             style="color: #fff;
                             background-color: #31b0d5;
                             border-color: #1b6d85;
                             height:40px; width:200px;
display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: 400;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
">Je m'inscris!</form:button>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center pt-20">
                <a href="#" onclick="chargerText();" class="btn-info btn">Peupler champs</a>
                &nbsp;
                <a href="/" class="btn-info btn">Retour accueil</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center pt-20">
                D�j� inscrit? <a href="login">Me connecter</a>
            </div>
        </div>


    </form:form>

</div>

</body>
</html>