<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Espace personnel</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
</head>
<body>

<div class="container-fluid">

    <div class="row pt-20">
        <div class="col-md-4 col-md-offset-4">

            <div class="text-center">
                <h1 class="form-signin-heading">K par K</h1>
                <h2 class="form-signin-heading">Espace clients</h2>
            </div>
            <h6 class=" pt-20">
                Bienvenue sur votre compte personnel ${personne.civilite.libelle} ${personne.nom}
            </h6>
            <table class="table table-bordered">
                <tr>
                    <td>
                        Pr�nom
                    </td>
                    <td>
                        <c:out value="${personne.prenom}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nom
                    </td>
                    <td>
                        <c:out value="${personne.prenom}"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        <c:out value="${personne.email}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <c:set var="dateOfBirth" value="${personne.dateDeNaissance}"/>
                        <c:set var="dayOB" value="${fn:substring(dateOfBirth, 8, 10)}"/>
                        <c:set var="monthOB" value="${fn:substring(dateOfBirth, 5, 7)}"/>
                        <c:set var="yearOB" value="${fn:substring(dateOfBirth, 0, 4)}"/>

                        Date de naissance
                    </td>
                    <td>
                        <c:out value="${dayOB}"/>.<c:out value="${monthOB}"/>.<c:out value="${yearOB}"/> <br/>
                    </td>
                </tr>

            </table>


            <br/>
            <a href="/demandePrix" class="btn-info btn">Demander un devis</a>
            <a href="/logout" class="btn btn-info">logout</a>
            <br/>
            <br/>




        </div>
    </div>

</div>
</body>
</html>