<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Connection</title>
    <script>
        function chargerText() {
            document.getElementById('email').value = "yannick@gmail.com";
            document.getElementById('motDePasse').value = "000000";
        }
    </script>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/signin.css"/>"/>
</head>
<body>

<div class="container">

    <form method="post" action="/login" class="form-signin">

<div class="text-center">
        <div class="btn-success"><c:out value="${message}"/></div>

        <h1 class="form-signin-heading">K par K</h1>
        <h2 class="form-signin-heading">Connectez vous</h2>
</div>
        <h6 class="form-signin-heading">Tous les champs sont obligatoires</h6>

        <label for="email" class="sr-only">Adresse email</label>
        <input type="text" name="email" id="email" class="form-control" required autofocus/>
        <br/>
        <label for="motDePasse" class="sr-only">Mot de passe</label>
        <input type="password" name="motDePasse" id="motDePasse" class="form-control" required/>
        <br/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <br/>
        <a href="#" onclick="chargerText();" class="btn-info btn">Peupler champs</a>
        &nbsp;
        <a href="/" class="btn-info btn">Retour accueil</a>

    </form>

</div> <!-- /container -->

</body>
</html>