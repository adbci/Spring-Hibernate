<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>K par K - Inscription client</title>
    <script>
        function chargerText() {
            document.getElementById('vitrage').value = "1";
            document.getElementById('hauteurEnCm').value = "120";
            document.getElementById('largeurEnCm').value = "80";
            document.getElementById('vitrage').value = "1";
            document.getElementById('couleur').value = "1";
            document.getElementById('materiau').value = "1";
        }
    </script>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
</head>
<body>

<div class="container-fluid">
    <form:form modelAttribute="demandePrixForm" method="post"
               cssClass="form-signin" action="/demandePrix">

        <div class="row pt-20">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h1 class="form-signin-heading">K par K</h1>
                <h2 class="form-signin-heading">Demande de devis</h2>
            </div>
            <div class="col-md-6 col-md-offset-3 pt-20">

                    <%-- client en cours (pas tr�s safe, je con�ois...
                    <form:hidden path="idClient"/>
                        --%>

                <form:select path="vitrage" cssClass="form-control">
                    <form:option value="0">Veuillez s�lectionner un vitrage</form:option>
                    <c:forEach var="vitrage" items="${vitrages}">
                        <form:option value="${vitrage.id}">${vitrage.nom}</form:option>
                    </c:forEach>
                </form:select>
                <div class="btn-warning">
                    <form:errors path="vitrage"/>
                </div>
                <br/>

                <form:input path="hauteurEnCm" placeHolder="Hauteur en cm" cssClass="form-control"/>
                <div class="btn-warning">
                    <form:errors path="hauteurEnCm"/>
                </div>
                <br/>

                <form:input path="largeurEnCm" placeHolder="Largeur en cm" cssClass="form-control"/>
                <div class="btn-warning">
                    <form:errors path="largeurEnCm"/>
                </div>
                <br/>


                <form:select path="couleur" cssClass="form-control">
                    <form:option value="0">Veuillez s�lectionner une couleur</form:option>
                    <c:forEach var="couleur" items="${couleurs}">
                        <form:option value="${couleur.id}">${couleur.nom}</form:option>
                    </c:forEach>
                </form:select>
                <div class="btn-warning">
                    <form:errors path="couleur"/>
                </div>
                <br/>

                <form:select path="materiau" cssClass="form-control">
                    <form:option value="0">Veuillez s�lectionner un mat�riau int�rieur</form:option>
                    <c:forEach var="materiau" items="${materiaux}">
                        <form:option value="${materiau.id}">${materiau.nom}</form:option>
                    </c:forEach>
                </form:select>
                <div class="btn-warning">
                    <form:errors path="materiau"/>
                </div>
                <br/>
                Pr�cisez votre demande si besoin<br/>
                <form:textarea cssClass="form-control" path="informationComplementaire" rows="5" cols="30"/>
                <br/>

            </div>

        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center pt-20">
                <form:button cssClass="btn btn-danger"
                             style="color: #fff;
                             background-color: #31b0d5;
                             border-color: #1b6d85;
                             height:40px; width:200px;
display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: 400;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
">Enregistrer ma demande</form:button>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center pt-20">
                <a href="#" onclick="chargerText();" class="btn-info btn">Peupler champs</a>
                &nbsp;
                <a href="/account" class="btn-info btn">Compte</a>
                &nbsp;
                <a href="/logout" class="btn-info btn">Logout</a>
            </div>
        </div>


    </form:form>

</div>

</body>
</html>