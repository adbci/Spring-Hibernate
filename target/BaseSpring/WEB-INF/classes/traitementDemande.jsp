<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>App datas</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
</head>
<body>

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12 text-center pt-20">

            <h1 class="form-signin-heading">K par K</h1>
            <h2 class="form-signin-heading">Espace d'administration experts</h2>

            Demande #<c:out value="${demande.id}"/>

            <br/><br/>

            <table class="table table-bordered">
                <tr>
                    <td>
                        Date/ heure demande
                    </td>
                    <td>
                        Demandeur
                    </td>
                    <td>
                        Matériaux
                    </td>
                    <td>
                        Vitrage
                    </td>
                    <td>
                        Couleur
                    </td>
                    <td>
                        Informations compélmentaires client
                    </td>
                </tr>
                <tr>
                    <td>
                        ${demande.dateDemande}

                    </td>
                    <td>
                        ${demande.personne.civilite}
                        ${demande.personne.nom}
                        ${demande.personne.prenom}
                    </td>
                    <td>
                        ${demande.materiau}
                    </td>
                    <td>
                        ${demande.vitrage}
                    </td>
                    <td class="bg-color-${demande.couleur}">

                        ${demande.couleur}

                        <%-- <c:set var="nb1" value="${gameToDisplay.number1}"/>
                                <c:set var="nb2" value="${gameToDisplay.number2}"/>
                                <c:set var="total" value="${nb1*nb2}"/>

                                <c:out value="${total}"/> --%>
                    </td>
                    <td>

                        ${demande.informationComplementaire}

                    </td>

                </tr>
                <tr>
                    <td colspan="2" class="text-center">&nbsp;
                    </td>
                    <td colspan="2" class="text-center">
                        <form:form modelAttribute="traitementDemandeForm" method="post"
                                   cssClass="form-signin" action="/traitementDemande">

Proposition de tarif
                            <form:input path="prix" cssStyle="text-align: center;"  placeHolder="Entrer prix ici" cssClass="form-control"/>

                            <form:button cssClass="btn btn-danger">VALIDER</form:button>
                        </form:form>
                    </td>
                    <td colspan="2" class="text-center">&nbsp;
                    </td>
                </tr>
            </table>

            <br/>
            <a href="/afficherDemandes?page=1&sort=id&sens=asc" class="btn btn-info">Retour demandes</a>
            <a href="/account" class="btn btn-info">Compte</a>
            <a href="/logout" class="btn btn-info">Logout</a>
        </div>
    </div>

</div>
</body>
</html>