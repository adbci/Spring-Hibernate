package fr.humanbooster.ab.fenetre.controller;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */

import fr.humanbooster.ab.fenetre.business.Civilite;
import fr.humanbooster.ab.fenetre.business.Client;
import fr.humanbooster.ab.fenetre.business.Personne;
import fr.humanbooster.ab.fenetre.service.CiviliteService;
import fr.humanbooster.ab.fenetre.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Controller
public class ClientController {

    @Autowired
    private CiviliteService civiliteService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private IndexController indexController;


    /**
     * méthode qui permet de convertir un idCivilite en objet civilite
     */
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        // field: "civilite" est le nom du champs du formulaire
        binder.registerCustomEditor(Civilite.class, "civilite", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                System.out.println("Civilite setAsText: " + text);
                setValue((text.equals("") ? null : civiliteService.findCiviliteById(Long.parseLong((String) text))));
            }

        });

    }

    /**
     * Methode qui enregistre un nouvel utilisateur (Client)
     * Spring verifie que toutes les données saisies dans le formulaire
     * sont conformes aux regles de gestion dans la classe métier
     * Pour effectuer cette validation, il faut un parametre BindingResult dans cette methode
     *
     * @param
     * @return
     */
    @RequestMapping(value = {"inscription"}, method = RequestMethod.POST)
    // je reçois un objet Client client, je le fais verifier par Spring
    public ModelAndView inscriptionPOST(@Valid @ModelAttribute(name = "inscriptionForm") Client client,
                                        BindingResult result) {
        ModelAndView mav = new ModelAndView();
        if (result.hasErrors()) {

            // les données saisies sur le form ne respectent pas les contraintes
            // exprimées dans la classe métier Player
            // on retourne sur la vue de l'inscription, ou sont displayed les erreurs
            mav.setViewName("/inscription");

            // recuperation des civilites
            List<Civilite> civiliteList = civiliteService.getAllCivilite();

            // mise a dispo dans la JSP
            mav.getModel().put("civilites", civiliteList);

        } else {
            // les contraintes sont respectées
            // on recupere les valeurs du form
            // pour peupler l'objet
            System.out.println("---- Civilite client: " + client.getCivilite().toString());

            // on procede à l'enregistrement du client
            clientService.addPersonne(client);

            // mise a dispo dans la JSP
            mav.getModel().put("client", client);

            // on redirige vers la page de login
            mav.setViewName("/inscrit");
        }
        return mav;
    }

    /**
     * Methode qui loggue un utilisateur (Personne)
     */
    @RequestMapping(value = {"login"}, method = RequestMethod.POST)
    public ModelAndView loginPOST(@RequestParam Map<String, Object> map) {
        ModelAndView mav = new ModelAndView();

        Personne personne = clientService.checkLogin(map.get("email").toString(), map.get("motDePasse").toString());

        if (personne == null) {
            // on redirige vers la page de'accueil
            mav.setViewName("connection");
        } else {
            // cree la session pour une heure
            httpSession.setAttribute("id", personne.getId());
            mav.getModel().put("client", personne);
            mav.setViewName("connecte");

            // test if expert or client
            // to display special menu options
            if(personne.getClass().getSimpleName().equals("Expert")){
                System.out.print(" FEU VERT, LA PATTE DE L'EXPERT!");
                mav.getModel().put("isExpert", true);
                mav.setViewName("connecteExpert");
            } else {
                System.out.print(" CLIENT!");
                mav.getModel().put("isExpert", false);
                mav.setViewName("connecte");
            }

        }
        return mav;
    }


    /**
     * Methode qui affiche le compte personnel d'un client
     */
    @RequestMapping(value = {"account"}, method = RequestMethod.GET)
    public ModelAndView account(@RequestParam Map<String, Object> map) throws ParseException {
        ModelAndView mav = new ModelAndView();

        // test de la session en cours
        if (httpSession.getAttribute("id") == null) {
            return indexController.login();
        }


        // on procede à la recupération des infos de l'utilisateur en cours
        Personne personne = clientService.findPersonneById(Long.parseLong(httpSession.getAttribute("id").toString()));

        mav.getModel().put("personne", personne);

        // test if expert or client
        // to display special menu options
        if(personne.getClass().getSimpleName().equals("Expert")){
            System.out.print(" FEU VERT, LA PATTE DE L'EXPERT!");
            mav.getModel().put("isExpert", true);
            mav.setViewName("accountExpert");
        } else {
            System.out.print(" CLIENT!");
            mav.getModel().put("isExpert", false);
            mav.setViewName("account");
        }
        return mav;
    }


    /**
     * Methode qui logout un utilisateur
     */
    @RequestMapping(value = {"logout"}, method = RequestMethod.GET)
    public ModelAndView logout() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("connection");

        // suppression de la session en cours
        httpSession.invalidate();

        mav.getModel().put("message", "Déconnection réussie!");
        return mav;
    }


}