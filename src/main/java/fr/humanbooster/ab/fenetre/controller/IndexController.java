package fr.humanbooster.ab.fenetre.controller;

import fr.humanbooster.ab.fenetre.service.CiviliteService;
import fr.humanbooster.ab.fenetre.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by Human Booster on 06/03/2017.
 */
@Controller
// classe qui traite les requetes http
// en remplacement des servlets précédemments écrites
public class IndexController {

    // injection d'une dépendance au controller test.java
    @Autowired // on declare un objet en précisant le nom d'une interface
    private ClientService clientService;

    @Autowired
    private CiviliteService civiliteService;

    @Autowired
    private DemandeController demandeController;

    @Autowired
    private HttpSession httpSession;

    // formulaire création de compte
    @RequestMapping(value = {"index", "/"}, method = RequestMethod.GET)
    public ModelAndView accueil() {
        ModelAndView mav = new ModelAndView();

        //test de la session en cours
        if (httpSession.getAttribute("id") != null) {
            return demandeController.demandeDevis();
        }

        // nom du fichier jsp
        mav.setViewName("inscription");

        // creer objet client
        mav.getModel().put("inscriptionForm", clientService.createClient());

        // creer liste des civilites
        mav.getModel().put("civilites", civiliteService.getAllCivilite());

        return mav;
    }


    // formulaire login
    @RequestMapping(value = {"login"}, method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView();

        // nom du fichier jsp
        mav.setViewName("connection");

        // creer objet client
        mav.getModel().put("loginForm", clientService.createClient());

        return mav;
    }

}