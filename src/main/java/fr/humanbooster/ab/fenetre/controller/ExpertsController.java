package fr.humanbooster.ab.fenetre.controller;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */

import fr.humanbooster.ab.fenetre.DateManager;
import fr.humanbooster.ab.fenetre.business.*;
import fr.humanbooster.ab.fenetre.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ExpertsController {

    @Autowired
    private DemandeService demandeService;

    @Autowired
    private VitrageService vitrageService;

    @Autowired
    private CouleurService couleurService;

    @Autowired
    private MateriauService materiauService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private IndexController indexController;

    /**
     * méthode qui permet de convertir un idXX en objet xx
     */
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        // field: "vitrage" est le nom du champs du formulaire
        binder.registerCustomEditor(Vitrage.class, "vitrage", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                System.out.println("Vitrage setAsText: " + text);
                setValue((text.equals("") ? null : vitrageService.findVitrageById(Long.parseLong((String) text))));
            }

        });

        // field: "couleur" est le nom du champs du formulaire
        binder.registerCustomEditor(Couleur.class, "couleur", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                System.out.println("Couleur setAsText: " + text);
                setValue((text.equals("") ? null : couleurService.findCouleurById(Long.parseLong((String) text))));
            }

        });

        // field: "materiau" est le nom du champs du formulaire
        binder.registerCustomEditor(Materiau.class, "materiau", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                System.out.println("Materiau setAsText: " + text);
                setValue((text.equals("") ? null : materiauService.findMateriauById(Long.parseLong((String) text))));
            }

        });
    }


    /**
     * Display demandes on multiple pages
     */
    @RequestMapping(value = {"afficherDemandes"}, method = RequestMethod.GET)
    public
    @ResponseBody
    ModelAndView displayDatasByPage(@RequestParam("page") String page,
                                    @RequestParam("sort") String sort,
                                    @RequestParam("sens") String sens) {
        ModelAndView mav = new ModelAndView();

        System.out.println(" #### DEBUG ####");

        // test de la session en cours
        if (httpSession.getAttribute("id") == null) {
            return indexController.login();
        }

        // on precise la vue
        mav.setViewName("affichageDemandes");

        // PAGINATION
        System.out.println("***** START PAGINATION ******");

        System.out.println(" =================================== ");
        System.out.println(" Quelle est la page retournée ? " + page);
        System.out.println(" =================================== ");
        mav.getModel().put("pageEnCours", page);

        // combien y a t'il de résultats?
        List<Demande> demandeList = demandeService.getAllDemande();
        int totalResults = demandeList.size();
        System.out.println(" Combien de résultats en tout? "+totalResults);
        mav.getModel().put("nbTotal", totalResults);

        // combien de résultats par page?
        int nbResultatsParPage = 5;
        System.out.println(" Combien de résultats par page? "+nbResultatsParPage);

        // combien de page cela fait il?
        int nbPages;
        if(totalResults%nbResultatsParPage>0) {
            nbPages = (totalResults / nbResultatsParPage)+1;
        } else {
            nbPages = (totalResults / nbResultatsParPage);
        }
        System.out.println(" Combien de pages au total? "+nbPages);
        mav.getModel().put("nbPages", nbPages);

        // creation de la navigation
        List<String> numeroPages = new ArrayList<>();
        for(int i = 1; i<=nbPages; i++) {
            numeroPages.add(String.valueOf(i));
            System.out.println("Ajout de la page #"+i);
        }
        mav.getModel().put("navigation", numeroPages);
        System.out.println(" Affichage de la navigation: "+numeroPages);

        mav.getModel().put("sort", sort);
        mav.getModel().put("sens", sens);

        // quelle plage de résultat afficher?
        int start = (Integer.parseInt(page)-1)*nbResultatsParPage;
        System.out.println("Quelle est la plage de résultats en cours? "+start);

        List<Demande> demandeToDisplay = null;

        if (start > 0 || String.valueOf(start).equals("")) {
            demandeToDisplay = demandeService.paginateDemande(start, nbResultatsParPage, sort, sens);

        } else {
            demandeToDisplay = demandeService.paginateDemande(0, nbResultatsParPage, sort, sens);
        }

        System.out.print("DEMANDES PAGINEES --> " + demandeToDisplay.toString());

        mav.getModel().put("demandesToDisplay", demandeToDisplay);

        System.out.println("***** END PAGINATION ******");

        return mav;
    }


    /**
     * Display demandes to process
     */
    @RequestMapping(value = {"traitementDemande"}, method = RequestMethod.POST)
    // je reçois un objet ??? ???, je le fais verifier par Spring
    public ModelAndView ajoutTarifPOST(@Valid @ModelAttribute(name = "traitementDemandeForm") Demande demande,
                                        BindingResult result) {
        ModelAndView mav = new ModelAndView();

            System.out.println("---- demande id: " + demande.getId().toString());

            // on procede à la MAJ de la demande
            demandeService.updateDemande(demande);

            // mise a dispo dans la JSP
            mav.getModel().put("demande", demande);
            mav.getModel().put("message", "Proposition effectué");

            // on redirige vers la page de login
            mav.setViewName("/traitementDemande");

        return mav;
    }


    /**
     * Process demands
     */
    @RequestMapping(value = {"ajoutTarif"}, method = RequestMethod.GET)
    public
    @ResponseBody
    ModelAndView afficherFormAjoutTarif(@RequestParam("demandeId") Long id) {
        ModelAndView mav = new ModelAndView();

        // test de la session en cours
        if (httpSession.getAttribute("id") == null) {
            return indexController.login();
        }


        // creer objet demande
        mav.getModel().put("demande", demandeService.findDemandeById(id));


        // on precise la vue
        mav.setViewName("traitementDemande");

        // recuperer infos demande
       Demande demande = demandeService.findDemandeById(id);
       System.out.print("QUelle est la demande renvoyée? "+demande.getId());
        mav.getModel().put("traitementDemandeForm", demande);

        return mav;
    }



    /**
     * Display processed demands on multiple pages
     */

    @RequestMapping(value = {"afficherDemandesTraitees"}, method = RequestMethod.GET)
    public
    @ResponseBody
    ModelAndView afficherDemandesTraiteesMultiPage(@RequestParam("page") String page,
                                    @RequestParam("sort") String sort,
                                    @RequestParam("sens") String sens) {
        ModelAndView mav = new ModelAndView();

        // test de la session en cours
        if (httpSession.getAttribute("id") == null) {
            return indexController.login();
        }

        // on precise la vue
        mav.setViewName("affichageDemandesTraitees");

        mav.getModel().put("pageEnCours", page);

        // combien y a t'il de résultats?
        List<Demande> demandeTraiteesList = demandeService.getAllDemandeTraitees();
        int totalResults = demandeTraiteesList.size();
        System.out.println(" Combien de résultats en tout? "+totalResults);
        mav.getModel().put("nbTotal", totalResults);

        // combien de résultats par page?
        int nbResultatsParPage = 5;
        System.out.println(" Combien de résultats par page? "+nbResultatsParPage);

        // combien de page cela fait il?
        int nbPages;
        if(totalResults%nbResultatsParPage>0) {
            nbPages = (totalResults / nbResultatsParPage)+1;
        } else {
            nbPages = (totalResults / nbResultatsParPage);
        }
        System.out.println(" Combien de pages au total? "+nbPages);
        mav.getModel().put("nbPages", nbPages);

        // creation de la navigation
        List<String> numeroPages = new ArrayList<>();
        for(int i = 1; i<=nbPages; i++) {
            numeroPages.add(String.valueOf(i));
            System.out.println("Ajout de la page #"+i);
        }
        mav.getModel().put("navigation", numeroPages);
        System.out.println(" Affichage de la navigation: "+numeroPages);

        mav.getModel().put("sort", sort);
        mav.getModel().put("sens", sens);

        // quelle plage de résultat afficher?
        int start = (Integer.parseInt(page)-1)*nbResultatsParPage;
        System.out.println("Quelle est la plage de résultats en cours? "+start);

        List<Demande> demandeTraiteesToDisplay = null;

        if (start > 0 || String.valueOf(start).equals("")) {
            demandeTraiteesToDisplay = demandeService.paginateDemandesTraitees(start, nbResultatsParPage, sort, sens);

        } else {
            demandeTraiteesToDisplay = demandeService.paginateDemandesTraitees(0, nbResultatsParPage, sort, sens);
        }

        System.out.print("DEMANDES PAGINEES --> " + demandeTraiteesToDisplay .toString());

        mav.getModel().put("demandesTraiteesToDisplay", demandeTraiteesToDisplay );


        return mav;
    }



}