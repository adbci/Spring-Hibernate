package fr.humanbooster.ab.fenetre.controller;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */

import fr.humanbooster.ab.fenetre.business.*;
import fr.humanbooster.ab.fenetre.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.util.List;

@Controller
public class DemandeController {

    @Autowired
    private DemandeService demandeService;

    @Autowired
    private VitrageService vitrageService;

    @Autowired
    private CouleurService couleurService;

    @Autowired
    private MateriauService materiauService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private IndexController indexController;

    /**
     * méthode qui permet de convertir un idXX en objet xx
     */
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        // field: "vitrage" est le nom du champs du formulaire
        binder.registerCustomEditor(Vitrage.class, "vitrage", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                System.out.println("Vitrage setAsText: " + text);
                setValue((text.equals("") ? null : vitrageService.findVitrageById(Long.parseLong((String) text))));
            }

        });

        // field: "couleur" est le nom du champs du formulaire
        binder.registerCustomEditor(Couleur.class, "couleur", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                System.out.println("Couleur setAsText: " + text);
                setValue((text.equals("") ? null : couleurService.findCouleurById(Long.parseLong((String) text))));
            }

        });

        // field: "materiau" est le nom du champs du formulaire
        binder.registerCustomEditor(Materiau.class, "materiau", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                System.out.println("Materiau setAsText: " + text);
                setValue((text.equals("") ? null : materiauService.findMateriauById(Long.parseLong((String) text))));
            }

        });
    }


    // formulaire demande devis
    @RequestMapping(value = {"demandePrix"}, method = RequestMethod.GET)
    public ModelAndView demandeDevis() {
        ModelAndView mav = new ModelAndView();

        // test de la session en cours
        if (httpSession.getAttribute("id") == null) {
           return indexController.login();
        }

        // nom du fichier jsp
        mav.setViewName("demandePrix");

        // recup id client en cours
        Long idCLient = Long.parseLong(httpSession.getAttribute("id").toString());

        // mise à dispo jsp
        mav.getModel().put("client", idCLient);

        // creer objet client
        mav.getModel().put("demandePrixForm", demandeService.createDemande());

        // recuperation des couleurs
        List<Couleur> couleurList = couleurService.getAllCouleur();

        // recuperation des materiaux
        List<Materiau> materiauList = materiauService.getAllMateriau();

        // recuperation des materiaux
        List<Vitrage> vitrageList = vitrageService.getAllVitrage();

        // mise a dispo dans la JSP
        mav.getModel().put("couleurs", couleurList);
        mav.getModel().put("materiaux", materiauList);
        mav.getModel().put("vitrages", vitrageList);

        return mav;
    }


    /**
     * Methode qui enregistre une demande
     * Spring verifie que toutes les données saisies dans le formulaire
     * sont conformes aux regles de gestion dans la classe métier
     * Pour effectuer cette validation, il faut un parametre BindingResult dans cette methode
     *
     */
    @RequestMapping(value = {"demandePrix"}, method = RequestMethod.POST)
    // je reçois un objet Client client, je le fais verifier par Spring
    public ModelAndView demandeDevisPOST(@Valid @ModelAttribute(name = "demandePrixForm") Demande demande,
                                        BindingResult result) {
        ModelAndView mav = new ModelAndView();
        if (result.hasErrors()) {

            mav.setViewName("/demandePrix");

            // recuperation des couleurs
            List<Couleur> couleurList = couleurService.getAllCouleur();

            // recuperation des materiaux
            List<Materiau> materiauList = materiauService.getAllMateriau();

            // recuperation des materiaux
            List<Vitrage> vitrageList = vitrageService.getAllVitrage();

            // mise a dispo dans la JSP
            mav.getModel().put("couleurs", couleurList);
            mav.getModel().put("materiaux", materiauList);
            mav.getModel().put("vitrages", vitrageList);

        } else {
            // les contraintes sont respectées
            // on recupere les valeurs du form
            System.out.println("---- Vitrage demande: " + demande.getVitrage().toString());

            // recup id client en cours
            Long idCLient = Long.parseLong(httpSession.getAttribute("id").toString());

            demande.setPersonne(clientService.findPersonneById(idCLient));

            // on procede à l'enregistrement de la demande
            demandeService.addDemande(demande);

            // mise a dispo dans la JSP
            mav.getModel().put("demande", demande);

            // on redirige vers la page de login
            mav.setViewName("/enregistre");
        }
        return mav;
    }

}