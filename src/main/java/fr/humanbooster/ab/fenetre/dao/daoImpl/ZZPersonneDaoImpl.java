        package fr.humanbooster.ab.fenetre.dao.daoImpl;

        import fr.humanbooster.ab.fenetre.business.Personne;
        import fr.humanbooster.ab.fenetre.dao.ZPersonneDao;
        import org.hibernate.SessionFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Repository;
        
        import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
        @Repository
        public class ZZPersonneDaoImpl implements ZPersonneDao {
            @Autowired
            private SessionFactory sf;

            @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
            public List<Personne> findAll() {
                System.out.print("Find all items for personne");
                return sf.getCurrentSession().createQuery("FROM Personne ").getResultList();
            }

            public Personne create(Personne personne) {
                System.out.print("Create a new personne");
                sf.getCurrentSession().save(personne);
                return personne;

            }

            public Personne findById(int idPersonne) {
                System.out.print("find a specific personne wit id: " + idPersonne);
                return sf.getCurrentSession().byId(Personne.class).load(idPersonne);
            }

            public Personne update(Personne personne) {
                System.out.print("update a specific personne");
                sf.getCurrentSession().update(personne);
                return personne;
            }

            public boolean delete(Personne personne) {
                System.out.print("delete a specific personne");
                sf.getCurrentSession().delete(personne);
                return true;
            }
        }
