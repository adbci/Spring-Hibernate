package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Demande;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface DemandeDao {

    public List<Demande> findAll();

    public List<Demande> findAllTraitees();

    public List<Demande> paginateDemande(int start, int limit, String sort, String sens);

    public List<Demande> paginateDemandeTraitees(int start, int limit, String sort, String sens);

    public Demande create(Demande demande);

    public Demande findById(Long idDemande);

    public Demande update(Demande demande);

    public boolean delete(Demande demande);
}
