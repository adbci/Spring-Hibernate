package fr.humanbooster.ab.fenetre.dao.daoImpl;

import fr.humanbooster.ab.fenetre.business.Vitrage;
import fr.humanbooster.ab.fenetre.dao.VitrageDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
@Repository
public class VitrageDaoImpl implements VitrageDao {
    @Autowired
    private SessionFactory sf;

    @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
    public List<Vitrage> findAll() {
        System.out.print("Find all items for vitrage");
        return sf.getCurrentSession().createQuery("FROM Vitrage ").getResultList();
    }

    public Vitrage create(Vitrage vitrage) {
        System.out.print("Create a new vitrage");
        sf.getCurrentSession().save(vitrage);
        return vitrage;

    }

    public Vitrage findById(Long idVitrage) {
        System.out.print("find a specific vitrage wit id: " + idVitrage);
        return sf.getCurrentSession().byId(Vitrage.class).load(idVitrage);
    }

    public Vitrage update(Vitrage vitrage) {
        System.out.print("update a specific vitrage");
        sf.getCurrentSession().update(vitrage);
        return vitrage;
    }

    public boolean delete(Vitrage vitrage) {
        System.out.print("delete a specific vitrage");
        sf.getCurrentSession().delete(vitrage);
        return true;
    }
}
