package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Vitrage;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
public interface VitrageDao {

    public List<Vitrage> findAll();

    public Vitrage create(Vitrage vitrage);

    public Vitrage findById(Long idVitrage);

    public Vitrage update(Vitrage vitrage);

    public boolean delete(Vitrage vitrage);
}
