package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Expert;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface ExpertDao {

    public List<Expert> findAll();

    public Expert create(Expert expert);

    public Expert findById(int idExpert);

    public Expert update(Expert expert);

    public boolean delete(Expert expert);
}
