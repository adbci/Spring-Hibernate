        package fr.humanbooster.ab.fenetre.dao.daoImpl;

        import fr.humanbooster.ab.fenetre.business.Expert;
        import fr.humanbooster.ab.fenetre.dao.ExpertDao;
        import org.hibernate.SessionFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Repository;
        
        import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
        @Repository
        public class ExpertDaoImpl implements ExpertDao {
            @Autowired
            private SessionFactory sf;

            @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
            public List<Expert> findAll() {
                System.out.print("Find all items for expert");
                return sf.getCurrentSession().createQuery("FROM Expert ").getResultList();
            }

            public Expert create(Expert expert) {
                System.out.print("Create a new expert");
                sf.getCurrentSession().save(expert);
                return expert;

            }

            public Expert findById(int idExpert) {
                System.out.print("find a specific expert wit id: " + idExpert);
                return sf.getCurrentSession().byId(Expert.class).load(idExpert);
            }

            public Expert update(Expert expert) {
                System.out.print("update a specific expert");
                sf.getCurrentSession().update(expert);
                return expert;
            }

            public boolean delete(Expert expert) {
                System.out.print("delete a specific expert");
                sf.getCurrentSession().delete(expert);
                return true;
            }
        }
