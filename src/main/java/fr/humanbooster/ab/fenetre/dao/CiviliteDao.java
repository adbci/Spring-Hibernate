package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Civilite;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface CiviliteDao {

    public List<Civilite> findAll();

    public Civilite create(Civilite civilite);

    public Civilite findById(Long idCivilite);

    public Civilite update(Civilite civilite);

    public boolean delete(Civilite civilite);
}
