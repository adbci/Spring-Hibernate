        package fr.humanbooster.ab.fenetre.dao.daoImpl;

        import fr.humanbooster.ab.fenetre.business.Civilite;
        import fr.humanbooster.ab.fenetre.dao.CiviliteDao;
        import org.hibernate.SessionFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Repository;
        
        import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
        @Repository
        public class CiviliteDaoImpl implements CiviliteDao {
            @Autowired
            private SessionFactory sf;

            @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
            public List<Civilite> findAll() {
                System.out.print("Find all items for civilite");
                return sf.getCurrentSession().createQuery("FROM Civilite ").getResultList();
            }

            public Civilite create(Civilite civilite) {
                System.out.print("Create a new civilite");
                sf.getCurrentSession().save(civilite);
                return civilite;

            }

            public Civilite findById(Long idCivilite) {
                System.out.print("find a specific civilite wit id: " + idCivilite);
                return sf.getCurrentSession().byId(Civilite.class).load(idCivilite);
            }

            public Civilite update(Civilite civilite) {
                System.out.print("update a specific civilite");
                sf.getCurrentSession().update(civilite);
                return civilite;
            }

            public boolean delete(Civilite civilite) {
                System.out.print("delete a specific civilite");
                sf.getCurrentSession().delete(civilite);
                return true;
            }
        }
