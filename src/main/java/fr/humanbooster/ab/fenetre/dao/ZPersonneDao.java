package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Personne;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
public interface ZPersonneDao {

    public List<Personne> findAll();

    public Personne create(Personne personne);

    public Personne findById(int idPersonne);

    public Personne update(Personne personne);

    public boolean delete(Personne personne);
}
