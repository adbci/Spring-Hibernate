        package fr.humanbooster.ab.fenetre.dao.daoImpl;

        import fr.humanbooster.ab.fenetre.business.Couleur;
        import fr.humanbooster.ab.fenetre.dao.CouleurDao;
        import org.hibernate.SessionFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Repository;
        
        import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
        @Repository
        public class CouleurDaoImpl implements CouleurDao {
            @Autowired
            private SessionFactory sf;

            @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
            public List<Couleur> findAll() {
                System.out.print("Find all items for couleur");
                return sf.getCurrentSession().createQuery("FROM Couleur ").getResultList();
            }

            public Couleur create(Couleur couleur) {
                System.out.print("Create a new couleur");
                sf.getCurrentSession().save(couleur);
                return couleur;

            }

            public Couleur findById(Long idCouleur) {
                System.out.print("find a specific couleur wit id: " + idCouleur);
                return sf.getCurrentSession().byId(Couleur.class).load(idCouleur);
            }

            public Couleur update(Couleur couleur) {
                System.out.print("update a specific couleur");
                sf.getCurrentSession().update(couleur);
                return couleur;
            }

            public boolean delete(Couleur couleur) {
                System.out.print("delete a specific couleur");
                sf.getCurrentSession().delete(couleur);
                return true;
            }
        }
