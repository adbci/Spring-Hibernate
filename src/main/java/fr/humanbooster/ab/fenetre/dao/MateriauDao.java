package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Materiau;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
public interface MateriauDao {

    public List<Materiau> findAll();

    public Materiau create(Materiau materiau);

    public Materiau findById(Long idMateriau);

    public Materiau update(Materiau materiau);

    public boolean delete(Materiau materiau);
}
