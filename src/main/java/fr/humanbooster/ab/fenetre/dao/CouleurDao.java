package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Couleur;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface CouleurDao {

    public List<Couleur> findAll();

    public Couleur create(Couleur couleur);

    public Couleur findById(Long idCouleur);

    public Couleur update(Couleur couleur);

    public boolean delete(Couleur couleur);
}
