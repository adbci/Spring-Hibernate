package fr.humanbooster.ab.fenetre.dao;

import fr.humanbooster.ab.fenetre.business.Client;
import fr.humanbooster.ab.fenetre.business.Personne;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface ClientDao {

    public List<Personne> findAll();

    public Personne checkLogin(String email, String motDePasse);

    public Personne create(Personne personne);

    public Personne findById(Long idPersonne);

    public Personne update(Personne personne);

    public boolean delete(Personne personne);
}
