        package fr.humanbooster.ab.fenetre.dao.daoImpl;

        import fr.humanbooster.ab.fenetre.business.Materiau;
        import fr.humanbooster.ab.fenetre.dao.MateriauDao;
        import org.hibernate.SessionFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Repository;
        
        import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
        @Repository
        public class MateriauDaoImpl implements MateriauDao {
            @Autowired
            private SessionFactory sf;

            @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
            public List<Materiau> findAll() {
                System.out.print("Find all items for materiau");
                return sf.getCurrentSession().createQuery("FROM Materiau ").getResultList();
            }

            public Materiau create(Materiau materiau) {
                System.out.print("Create a new materiau");
                sf.getCurrentSession().save(materiau);
                return materiau;

            }

            public Materiau findById(Long idMateriau) {
                System.out.print("find a specific materiau wit id: " + idMateriau);
                return sf.getCurrentSession().byId(Materiau.class).load(idMateriau);
            }

            public Materiau update(Materiau materiau) {
                System.out.print("update a specific materiau");
                sf.getCurrentSession().update(materiau);
                return materiau;
            }

            public boolean delete(Materiau materiau) {
                System.out.print("delete a specific materiau");
                sf.getCurrentSession().delete(materiau);
                return true;
            }
        }
