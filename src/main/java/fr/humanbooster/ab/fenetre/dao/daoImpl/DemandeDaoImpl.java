package fr.humanbooster.ab.fenetre.dao.daoImpl;

import fr.humanbooster.ab.fenetre.business.Demande;
import fr.humanbooster.ab.fenetre.dao.DemandeDao;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
@Repository
public class DemandeDaoImpl implements DemandeDao {
    @Autowired
    private SessionFactory sf;

    @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
    public List<Demande> findAll() {
        System.out.print("Find all items for demande");
        return sf.getCurrentSession().createQuery("FROM Demande ").getResultList();
    }

    @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
    public List<Demande> findAllTraitees() {
        System.out.print("Find all items for demandes traitees");
        return sf.getCurrentSession().createQuery("FROM Demande WHERE dateReponse is not null ").getResultList();
    }


    @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
    public List<Demande> paginateDemande(int start, int limit, String sort, String sens) {
        /*
        return sf.getCurrentSession().createQuery("FROM City LIMIT "+start+", "+limit).getResultList();
         */

        System.out.print("Find all items for demande starting from " + start + " and limited to " + limit);

        System.out.println(" **** DANS LA DAO  ***** ");

        Query query = sf.getCurrentSession().createQuery("from Demande order by " + sort + " " + sens);
        query.setFirstResult(start);
        query.setMaxResults(limit);
        List<Demande> demandeList = query.getResultList();

        for (Demande demande : demandeList) {
            // test boucle
            System.out.println(
                    "Id: " + demande.getId()
                            + ", Matériau: " + demande.getMateriau()
            );
        }

        System.out.println(" **** DANS LE CONTROLLEUR ***** Demandes paginées: " + demandeList.size());

        return demandeList;

    }

    @SuppressWarnings("unchecked") // permet de supprimer les alerts liées au HQL dessous
    public List<Demande> paginateDemandeTraitees(int start, int limit, String sort, String sens) {

        System.out.print("Find all items for demandes traitees starting from " + start + " and limited to " + limit);

        System.out.println(" **** DANS LA DAO  ***** ");

        Query query = sf.getCurrentSession().createQuery("from Demande WHERE dateReponse is not null order by " + sort + " " + sens);
        query.setFirstResult(start);
        query.setMaxResults(limit);
        List<Demande> demandeTraiteesList = query.getResultList();

        for (Demande demande : demandeTraiteesList) {
            // test boucle
            System.out.println(
                    "Id: " + demande.getId()
                            + ", Matériau: " + demande.getMateriau()
            );
        }

        System.out.println(" **** DANS LE CONTROLLEUR ***** Demandes paginées: " + demandeTraiteesList.size());

        return demandeTraiteesList;

    }

    public Demande create(Demande demande) {
        System.out.print("Create a new demande");
        sf.getCurrentSession().save(demande);
        return demande;

    }

    public Demande findById(Long idDemande) {
        System.out.print("find a specific demande wit id: " + idDemande);
        return sf.getCurrentSession().byId(Demande.class).load(idDemande);
    }

    public Demande update(Demande demande) {
        System.out.print("update a specific demande");
        sf.getCurrentSession().update(demande);
        return demande;
    }

    public boolean delete(Demande demande) {
        System.out.print("delete a specific demande");
        sf.getCurrentSession().delete(demande);
        return true;
    }
}
