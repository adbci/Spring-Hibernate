package fr.humanbooster.ab.fenetre;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Service {

    private String pattern;

    String firstLower(String word) {

        return Character.toLowerCase(word.charAt(0)) + word.substring(1);
    }

    String createService(String path, String serviceName) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        pattern = "" +
                "package "+path.replace("\\", ".")+".service;\n" +
                "import "+path.replace("\\", ".")+".business."+serviceName+";\n" +
                "\n" +
                "import java.util.List;\n" +
                "\n" +
                "/**\n" +
                " * Created by "+System.getProperty("user.name")+" on "+dateFormat.format(date)+".\n" +
                " */\n" +
                "public interface "+serviceName+"Service {\n" +
                "\n" +
                "    public List<"+serviceName+"> getAll"+serviceName+"();\n" +
                "\n" +
                "    public "+serviceName+" add"+serviceName+"("+serviceName+" "+firstLower(serviceName)+");\n" +
                "\n" +
                "    public "+serviceName+" update"+serviceName+"("+serviceName+" "+firstLower(serviceName)+");\n" +
                "\n" +
                "    public "+serviceName+" find"+serviceName+"ById(int id"+serviceName+");\n" +
                "\n" +
                "    public boolean delete"+serviceName+"("+serviceName+" "+firstLower(serviceName)+");\n" +
                "\n" +
                "}";

        System.out.println(pattern);
        return pattern;
    }


    String createServiceImpl(String path, String serviceName) {


        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String pattern = "" +
                "package " + path.replace("\\", ".") + ".service.serviceImpl;\n" +
                "\n" +
                "import " + path.replace("\\", ".") + ".business." + serviceName + ";\n" +
                "import " + path.replace("\\", ".") + ".dao." + serviceName + "Dao;\n" +
                "import " + path.replace("\\", ".") + ".service." + serviceName + "Service;\n" +
                "import org.springframework.beans.factory.annotation.Autowired;\n" +
                "import org.springframework.stereotype.Service;\n" +
                "import org.springframework.transaction.annotation.Transactional;\n" +
                "\n" +
                "import java.util.List;\n" +
                "/**\n" +
                " * Created by "+System.getProperty("user.name")+" on "+dateFormat.format(date)+".\n" +
                " */\n" +
                "\n" +
                "@Service\n" +
                "@Transactional // pour que Spring se charge des transactions au niveau des services\n" +
                "public class " + serviceName + "ServiceImpl implements " + serviceName + "Service {\n" +
                "\n" +
                "    @Autowired\n" +
                "    private " + serviceName + "Dao " + firstLower(serviceName) + "Dao;\n" +
                "\n" +
                "    @Transactional(readOnly = true)\n" +
                "    public List<" + serviceName + "> getAll" + serviceName + "() {\n" +
                "        return " + firstLower(serviceName) + "Dao.findAll();\n" +
                "    }\n" +
                "\n" +
                "    public " + serviceName + " add" + serviceName + "(" + serviceName + " " + firstLower(serviceName) + ") {\n" +
                "        " + firstLower(serviceName) + " = " + firstLower(serviceName) + "Dao.create(" + firstLower(serviceName) + ");\n" +
                "        return " + firstLower(serviceName) + ";\n" +
                "    }\n" +
                "\n" +
                "    public " + serviceName + " find" + serviceName + "ById(int id" + serviceName + "){\n" +
                "        " + serviceName + " " + firstLower(serviceName) + " = " + firstLower(serviceName) + "Dao.findById(id" + serviceName + ");\n" +
                "        return " + firstLower(serviceName) + ";\n" +
                "    }\n" +
                "\n" +
                "    public " + serviceName + " update" + serviceName + "(" + serviceName + " " + firstLower(serviceName) + ") {\n" +
                "        " + firstLower(serviceName) + " = " + firstLower(serviceName) + "Dao.update(" + firstLower(serviceName) + ");\n" +
                "        return " + firstLower(serviceName) + ";\n" +
                "    }\n" +
                "\n" +
                "    public boolean delete" + serviceName + "(" + serviceName + " " + firstLower(serviceName) + "){\n" +
                "        boolean result = " + firstLower(serviceName) + "Dao.delete(" + firstLower(serviceName) + ");\n" +
                "        return result;\n" +
                "    }\n" +
                "\n" +
                "}\n";

        System.out.println(pattern);
        return pattern;
    }


}
