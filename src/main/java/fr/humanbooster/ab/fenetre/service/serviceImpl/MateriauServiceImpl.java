package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Materiau;
import fr.humanbooster.ab.fenetre.dao.MateriauDao;
import fr.humanbooster.ab.fenetre.service.MateriauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class MateriauServiceImpl implements MateriauService {

    @Autowired
    private MateriauDao materiauDao;

    @Transactional(readOnly = true)
    public List<Materiau> getAllMateriau() {
        return materiauDao.findAll();
    }

    public Materiau addMateriau(Materiau materiau) {
        materiau = materiauDao.create(materiau);
        return materiau;
    }

    public Materiau findMateriauById(Long idMateriau){
        Materiau materiau = materiauDao.findById(idMateriau);
        return materiau;
    }

    public Materiau updateMateriau(Materiau materiau) {
        materiau = materiauDao.update(materiau);
        return materiau;
    }

    public boolean deleteMateriau(Materiau materiau){
        boolean result = materiauDao.delete(materiau);
        return result;
    }

}
