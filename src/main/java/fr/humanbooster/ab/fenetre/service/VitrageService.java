package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Vitrage;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
public interface VitrageService {

    public List<Vitrage> getAllVitrage();

    public Vitrage addVitrage(Vitrage vitrage);

    public Vitrage updateVitrage(Vitrage vitrage);

    public Vitrage findVitrageById(Long idVitrage);

    public boolean deleteVitrage(Vitrage vitrage);

}