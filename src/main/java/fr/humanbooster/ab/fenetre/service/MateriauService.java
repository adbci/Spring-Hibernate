package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Materiau;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
public interface MateriauService {

    public List<Materiau> getAllMateriau();

    public Materiau addMateriau(Materiau materiau);

    public Materiau updateMateriau(Materiau materiau);

    public Materiau findMateriauById(Long idMateriau);

    public boolean deleteMateriau(Materiau materiau);

}