package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Vitrage;
import fr.humanbooster.ab.fenetre.dao.VitrageDao;
import fr.humanbooster.ab.fenetre.service.VitrageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class VitrageServiceImpl implements VitrageService {

    @Autowired
    private VitrageDao vitrageDao;

    @Transactional(readOnly = true)
    public List<Vitrage> getAllVitrage() {
        return vitrageDao.findAll();
    }

    public Vitrage addVitrage(Vitrage vitrage) {
        vitrage = vitrageDao.create(vitrage);
        return vitrage;
    }

    public Vitrage findVitrageById(Long idVitrage){
        Vitrage vitrage = vitrageDao.findById(idVitrage);
        return vitrage;
    }

    public Vitrage updateVitrage(Vitrage vitrage) {
        vitrage = vitrageDao.update(vitrage);
        return vitrage;
    }

    public boolean deleteVitrage(Vitrage vitrage){
        boolean result = vitrageDao.delete(vitrage);
        return result;
    }

}
