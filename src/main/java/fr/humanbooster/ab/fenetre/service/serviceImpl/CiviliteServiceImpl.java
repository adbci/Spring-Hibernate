package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Civilite;
import fr.humanbooster.ab.fenetre.dao.CiviliteDao;
import fr.humanbooster.ab.fenetre.service.CiviliteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class CiviliteServiceImpl implements CiviliteService {

    @Autowired
    private CiviliteDao civiliteDao;

    @Transactional(readOnly = true)
    public List<Civilite> getAllCivilite() {
        return civiliteDao.findAll();
    }

    public Civilite addCivilite(Civilite civilite) {
        civilite = civiliteDao.create(civilite);
        return civilite;
    }

    public Civilite findCiviliteById(Long idCivilite){
        Civilite civilite = civiliteDao.findById(idCivilite);
        return civilite;
    }

    public Civilite updateCivilite(Civilite civilite) {
        civilite = civiliteDao.update(civilite);
        return civilite;
    }

    public boolean deleteCivilite(Civilite civilite){
        boolean result = civiliteDao.delete(civilite);
        return result;
    }

}
