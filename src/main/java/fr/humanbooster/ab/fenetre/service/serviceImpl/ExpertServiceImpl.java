package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Expert;
import fr.humanbooster.ab.fenetre.dao.ExpertDao;
import fr.humanbooster.ab.fenetre.service.ExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class ExpertServiceImpl implements ExpertService {

    @Autowired
    private ExpertDao expertDao;

    @Transactional(readOnly = true)
    public List<Expert> getAllExpert() {
        return expertDao.findAll();
    }

    public Expert addExpert(Expert expert) {
        expert = expertDao.create(expert);
        return expert;
    }

    public Expert findExpertById(int idExpert){
        Expert expert = expertDao.findById(idExpert);
        return expert;
    }

    public Expert updateExpert(Expert expert) {
        expert = expertDao.update(expert);
        return expert;
    }

    public boolean deleteExpert(Expert expert){
        boolean result = expertDao.delete(expert);
        return result;
    }

}
