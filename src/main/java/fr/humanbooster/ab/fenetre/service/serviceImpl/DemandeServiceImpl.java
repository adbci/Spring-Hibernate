package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Client;
import fr.humanbooster.ab.fenetre.business.Demande;
import fr.humanbooster.ab.fenetre.dao.DemandeDao;
import fr.humanbooster.ab.fenetre.service.DemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class DemandeServiceImpl implements DemandeService {

    @Autowired
    private DemandeDao demandeDao;

    @Transactional(readOnly = true)
    public List<Demande> getAllDemande() {
        return demandeDao.findAll();
    }

    @Transactional(readOnly = true)
    public List<Demande> getAllDemandeTraitees() {
        return demandeDao.findAllTraitees();
    }

    public Demande createDemande(){
        return new Demande();
    }

    @Transactional(readOnly = true)
    public List<Demande> paginateDemande(int start, int limit, String sort, String sens) {
        return demandeDao.paginateDemande(start, limit, sort, sens);
    }

    @Transactional(readOnly = true)
    public List<Demande> paginateDemandesTraitees(int start, int limit, String sort, String sens) {
        return demandeDao.paginateDemandeTraitees(start, limit, sort, sens);
    }

    public Demande addDemande(Demande demande) {
        demande = demandeDao.create(demande);
        return demande;
    }

    public Demande findDemandeById(Long idDemande){
        Demande demande = demandeDao.findById(idDemande);
        return demande;
    }

    public Demande updateDemande(Demande demande) {
        demande = demandeDao.update(demande);
        return demande;
    }

    public boolean deleteDemande(Demande demande){
        boolean result = demandeDao.delete(demande);
        return result;
    }

}
