package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Client;
import fr.humanbooster.ab.fenetre.business.Expert;
import fr.humanbooster.ab.fenetre.business.Personne;
import fr.humanbooster.ab.fenetre.dao.ClientDao;
import fr.humanbooster.ab.fenetre.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientDao clientDao;

    @Transactional(readOnly = true)
    public List<Personne> getAllPersonne() {
        return clientDao.findAll();
    }

    public Personne checkLogin(String email, String motDePasse) {
        return clientDao.checkLogin(email, motDePasse);
    }

    public Personne createClient(){
        return new Client();
    }

    public Personne createExpert(){
        return new Expert();
    }

    public Personne addPersonne(Personne personne) {
        personne = clientDao.create(personne);
        return personne;
    }

    public Personne findPersonneById(Long idPersonne){
        Personne personne = clientDao.findById(idPersonne);
        return personne;
    }

    public Personne updatePersonne(Personne personne) {
        personne = clientDao.update(personne);
        return personne;
    }

    public boolean deletePersonne(Personne personne){
        boolean result = clientDao.delete(personne);
        return result;
    }

}
