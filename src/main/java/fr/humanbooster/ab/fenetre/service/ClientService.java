package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Client;
import fr.humanbooster.ab.fenetre.business.Expert;
import fr.humanbooster.ab.fenetre.business.Personne;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface ClientService {

    public List<Personne> getAllPersonne();

    public Personne checkLogin(String email, String motDePasse);

    public Personne createClient();

    public Personne createExpert();

    public Personne addPersonne(Personne personne);

    public Personne updatePersonne(Personne personne);

    public Personne findPersonneById(Long idPersonne);

    public boolean deletePersonne(Personne personne);

}