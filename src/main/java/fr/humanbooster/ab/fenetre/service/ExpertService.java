package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Expert;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
public interface ExpertService {

    public List<Expert> getAllExpert();

    public Expert addExpert(Expert expert);

    public Expert updateExpert(Expert expert);

    public Expert findExpertById(int idExpert);

    public boolean deleteExpert(Expert expert);

}