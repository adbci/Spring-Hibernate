package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Couleur;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface CouleurService {

    public List<Couleur> getAllCouleur();

    public Couleur addCouleur(Couleur couleur);

    public Couleur updateCouleur(Couleur couleur);

    public Couleur findCouleurById(Long idCouleur);

    public boolean deleteCouleur(Couleur couleur);

}