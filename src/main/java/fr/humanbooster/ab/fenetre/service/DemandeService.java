package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Client;
import fr.humanbooster.ab.fenetre.business.Demande;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface DemandeService {

    public List<Demande> getAllDemande();

    public List<Demande> getAllDemandeTraitees();

    public Demande createDemande();

    public List<Demande> paginateDemande(int start, int limit, String sort, String sens);

    public List<Demande> paginateDemandesTraitees(int start, int limit, String sort, String sens);

    public Demande addDemande(Demande demande);

    public Demande updateDemande(Demande demande);

    public Demande findDemandeById(Long idDemande);

    public boolean deleteDemande(Demande demande);

}