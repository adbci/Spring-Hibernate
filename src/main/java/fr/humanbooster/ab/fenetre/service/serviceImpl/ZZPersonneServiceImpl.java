package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Personne;
import fr.humanbooster.ab.fenetre.dao.ZPersonneDao;
import fr.humanbooster.ab.fenetre.service.ZPersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class ZZPersonneServiceImpl implements ZPersonneService {

    @Autowired
    private ZPersonneDao ZPersonneDao;

    @Transactional(readOnly = true)
    public List<Personne> getAllPersonne() {
        return ZPersonneDao.findAll();
    }

    public Personne addPersonne(Personne personne) {
        personne = ZPersonneDao.create(personne);
        return personne;
    }

    public Personne findPersonneById(int idPersonne){
        Personne personne = ZPersonneDao.findById(idPersonne);
        return personne;
    }

    public Personne updatePersonne(Personne personne) {
        personne = ZPersonneDao.update(personne);
        return personne;
    }

    public boolean deletePersonne(Personne personne){
        boolean result = ZPersonneDao.delete(personne);
        return result;
    }

}
