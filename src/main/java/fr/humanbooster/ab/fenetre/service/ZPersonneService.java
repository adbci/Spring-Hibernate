package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Personne;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:02.
 */
public interface ZPersonneService {

    public List<Personne> getAllPersonne();

    public Personne addPersonne(Personne personne);

    public Personne updatePersonne(Personne personne);

    public Personne findPersonneById(int idPersonne);

    public boolean deletePersonne(Personne personne);

}