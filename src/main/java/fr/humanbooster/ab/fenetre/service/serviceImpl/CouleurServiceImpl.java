package fr.humanbooster.ab.fenetre.service.serviceImpl;

import fr.humanbooster.ab.fenetre.business.Couleur;
import fr.humanbooster.ab.fenetre.dao.CouleurDao;
import fr.humanbooster.ab.fenetre.service.CouleurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class CouleurServiceImpl implements CouleurService {

    @Autowired
    private CouleurDao couleurDao;

    @Transactional(readOnly = true)
    public List<Couleur> getAllCouleur() {
        return couleurDao.findAll();
    }

    public Couleur addCouleur(Couleur couleur) {
        couleur = couleurDao.create(couleur);
        return couleur;
    }

    public Couleur findCouleurById(Long idCouleur){
        Couleur couleur = couleurDao.findById(idCouleur);
        return couleur;
    }

    public Couleur updateCouleur(Couleur couleur) {
        couleur = couleurDao.update(couleur);
        return couleur;
    }

    public boolean deleteCouleur(Couleur couleur){
        boolean result = couleurDao.delete(couleur);
        return result;
    }

}
