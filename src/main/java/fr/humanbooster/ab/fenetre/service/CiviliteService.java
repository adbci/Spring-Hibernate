package fr.humanbooster.ab.fenetre.service;
import fr.humanbooster.ab.fenetre.business.Civilite;

import java.util.List;

/**
 * Created by Human Booster on 2017/03/23 12:38:01.
 */
public interface CiviliteService {

    public List<Civilite> getAllCivilite();

    public Civilite addCivilite(Civilite civilite);

    public Civilite updateCivilite(Civilite civilite);

    public Civilite findCiviliteById(Long idCivilite);

    public boolean deleteCivilite(Civilite civilite);

}