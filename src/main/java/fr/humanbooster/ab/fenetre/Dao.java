package fr.humanbooster.ab.fenetre;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dao {

    private String pattern;

    String firstLower(String word) {

        return Character.toLowerCase(word.charAt(0)) + word.substring(1);
    }

    String createDao(String path, String daoName) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        pattern = "" +
                "package " + path.replace("\\", ".") + ".dao;\n" +
                "\n" +
                "import " + path.replace("\\", ".") +".business."+ daoName + ";\n" +
                "\n" +
                "import java.util.List;\n" +
                "\n" +
                "/**\n" +
                " * Created by "+System.getProperty("user.name")+" on "+dateFormat.format(date)+".\n" +
                " */\n" +
                "public interface " + daoName + "Dao {\n" +
                "\n" +
                "    public List<" + daoName + "> findAll();\n" +
                "\n" +
                "    public " + daoName + " create(" + daoName + " " + firstLower(daoName) + ");\n" +
                "\n" +
                "    public " + daoName + " findById(int id" + daoName + ");\n" +
                "\n" +
                "    public " + daoName + " update(" + daoName + " " + firstLower(daoName) + ");\n" +
                "\n" +
                "    public boolean delete(" + daoName + " " + firstLower(daoName) + ");\n" +
                "}\n";

        System.out.println(pattern);
        return pattern;
    }


    String createDaoImpl(String path, String daoName) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String pattern = "" +
                "        package " + path.replace("\\", ".") + ".dao.daoImpl;\n" +
                "\n" +
                "        import " + path.replace("\\", ".") + ".business."+daoName+";\n" +
                "        import " + path.replace("\\", ".") + ".dao."+daoName+"Dao;\n" +
                "        import org.hibernate.SessionFactory;\n" +
                "        import org.springframework.beans.factory.annotation.Autowired;\n" +
                "        import org.springframework.stereotype.Repository;\n" +
                "        \n" +
                "        import java.util.List;\n" +
                "\n" +
                "/**\n" +
                " * Created by "+System.getProperty("user.name")+" on "+dateFormat.format(date)+".\n" +
                " */\n" +
                "        @Repository\n" +
                "        public class "+daoName+"DaoImpl implements "+daoName+"Dao {\n" +
                "            @Autowired\n" +
                "            private SessionFactory sf;\n" +
                "\n" +
                "            @SuppressWarnings(\"unchecked\") // permet de supprimer les alerts liées au HQL dessous\n" +
                "            public List<"+daoName+"> findAll() {\n" +
                "                System.out.print(\"Find all items for " + firstLower(daoName) + "\");\n" +
                "                return sf.getCurrentSession().createQuery(\"FROM "+daoName+" \").getResultList();\n" +
                "            }\n" +
                "\n" +
                "            public "+daoName+" create("+daoName+" " + firstLower(daoName) + ") {\n" +
                "                System.out.print(\"Create a new " + firstLower(daoName) + "\");\n" +
                "                sf.getCurrentSession().save(" + firstLower(daoName) + ");\n" +
                "                return " + firstLower(daoName) + ";\n" +
                "\n" +
                "            }\n" +
                "\n" +
                "            public "+daoName+" findById(int id"+daoName+") {\n" +
                "                System.out.print(\"find a specific " + firstLower(daoName) + " wit id: \" + id"+daoName+");\n" +
                "                return sf.getCurrentSession().byId("+daoName+".class).load(id"+daoName+");\n" +
                "            }\n" +
                "\n" +
                "            public "+daoName+" update("+daoName+" " + firstLower(daoName) + ") {\n" +
                "                System.out.print(\"update a specific " + firstLower(daoName) + "\");\n" +
                "                sf.getCurrentSession().update(" + firstLower(daoName) + ");\n" +
                "                return " + firstLower(daoName) + ";\n" +
                "            }\n" +
                "\n" +
                "            public boolean delete("+daoName+" " + firstLower(daoName) + ") {\n" +
                "                System.out.print(\"delete a specific " + firstLower(daoName) + "\");\n" +
                "                sf.getCurrentSession().delete(" + firstLower(daoName) + ");\n" +
                "                return true;\n" +
                "            }\n" +
                "        }\n" +
                "";

        System.out.println(pattern);
        return pattern;
    }


}
