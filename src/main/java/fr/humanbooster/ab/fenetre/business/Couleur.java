package fr.humanbooster.ab.fenetre.business;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */
@Entity
public class Couleur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String codeRgb;

    @OneToMany(mappedBy = "couleur")
    private List<Demande> demandeList;

    public Couleur() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodeRgb() {
        return codeRgb;
    }

    public void setCodeRgb(String codeRgb) {
        this.codeRgb = codeRgb;
    }

    public List<Demande> getDemandeList() {
        return demandeList;
    }

    public void setDemandeList(List<Demande> demandeList) {
        this.demandeList = demandeList;
    }

    @Override
    public String toString() {
        return nom;
    }
}
