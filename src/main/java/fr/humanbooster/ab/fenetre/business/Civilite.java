package fr.humanbooster.ab.fenetre.business;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */
@Entity
public class Civilite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String libelle;

    @OneToMany(mappedBy = "civilite")
    private List<Personne> personnes;

    public Civilite() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<Personne> getPersonnes() {
        return personnes;
    }

    public void setPersonnes(List<Personne> personnes) {
        this.personnes = personnes;
    }

    @Override
    public String toString() {
        return libelle;
    }
}
