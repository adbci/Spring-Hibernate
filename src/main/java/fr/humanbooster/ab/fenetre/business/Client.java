package fr.humanbooster.ab.fenetre.business;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */
@Entity
public class Client extends Personne {

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @NotNull(message = "Saisir la date de naissance")
    private Date dateDeNaissance;

    public Client() {
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }
}
