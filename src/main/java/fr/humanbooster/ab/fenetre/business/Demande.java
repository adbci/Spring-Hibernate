package fr.humanbooster.ab.fenetre.business;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */
@Entity
public class Demande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date dateDemande = new Date();
    private Date dateReponse;
    @Min(value=30, message = "La dimension doit être de 30cm minimum")
    @NotNull(message = "Merci de préciser la largeur")
    private Float largeurEnCm;
    @Min(value=30, message = "La dimension doit être de 30cm minimum")
    @NotNull(message = "Merci de préciser la hauteur")
    private Float hauteurEnCm;
    private Float prix;
    private String informationComplementaire;

    @ManyToOne
    @NotNull(message = "Merci de choisir un matériau svp")
    private Materiau materiau;

    @ManyToOne
    @NotNull(message = "Merci de choisir une couleur svp")
    private Couleur couleur;

    @ManyToOne
    @NotNull(message = "Veuillez sélectionner un vitrage svp")
    private Vitrage vitrage;

    @ManyToOne
    private Personne personne;

    public Demande() {
        largeurEnCm = 0F;
        hauteurEnCm = 0F;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(Date dateDemande) {
        this.dateDemande = dateDemande;
    }

    public Date getDateReponse() {
        return dateReponse;
    }

    public void setDateReponse(Date dateReponse) {
        this.dateReponse = dateReponse;
    }

    public Float getLargeurEnCm() {
        return largeurEnCm;
    }

    public void setLargeurEnCm(Float largeurEnCm) {
        this.largeurEnCm = largeurEnCm;
    }

    public Float getHauteurEnCm() {
        return hauteurEnCm;
    }

    public void setHauteurEnCm(Float hauteurEnCm) {
        this.hauteurEnCm = hauteurEnCm;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public String getInformationComplementaire() {
        return informationComplementaire;
    }

    public void setInformationComplementaire(String informationComplementaire) {
        this.informationComplementaire = informationComplementaire;
    }

    public Materiau getMateriau() {
        return materiau;
    }

    public void setMateriau(Materiau materiau) {
        this.materiau = materiau;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public Vitrage getVitrage() {
        return vitrage;
    }

    public void setVitrage(Vitrage vitrage) {
        this.vitrage = vitrage;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }
}
