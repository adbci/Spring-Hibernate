package fr.humanbooster.ab.fenetre.business;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */
@Entity
public class Materiau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;

    @OneToMany(mappedBy = "materiau")
    private List<Demande> demandeList;

    public Materiau() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Demande> getDemandeList() {
        return demandeList;
    }

    public void setDemandeList(List<Demande> demandeList) {
        this.demandeList = demandeList;
    }

    @Override
    public String toString() {
        return nom;
    }
}
