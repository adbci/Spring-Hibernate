package fr.humanbooster.ab.fenetre.business;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */
@Entity
public class Expert extends Personne {

    private Date dateEmbauche;

    public Expert() {
    }

    public Date getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(Date dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }


}
