package fr.humanbooster.ab.fenetre.business;

import com.sun.istack.internal.NotNull;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Adrien Bucci on 23/03/2017.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personne {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Saisir votre nom svp")
    private String nom;
    @NotBlank(message = "Saisir votre prénom svp")
    private String prenom;
    @NotBlank(message = "Saisir votre email svp")
    private String email;
    @NotBlank(message = "Saisir votre mot de passe svp")
    @Size(min = 5, message = "Votre mot de passe doit contenir au moins 5 caractères!")
    private String motDePasse;

    @OneToMany(mappedBy = "personne")
    private List<Demande> demandeList;

    @ManyToOne
    @javax.validation.constraints.NotNull(message = "Merci de choisir votre civilité")
    private Civilite civilite;

    public Personne() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public List<Demande> getDemandeList() {
        return demandeList;
    }

    public void setDemandeList(List<Demande> demandeList) {
        this.demandeList = demandeList;
    }

    public Civilite getCivilite() {
        return civilite;
    }

    public void setCivilite(Civilite civilite) {
        this.civilite = civilite;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", motDePasse='" + motDePasse + '\'' +
                ", demandeList=" + demandeList +
                ", civilite=" + civilite +
                '}';
    }
}
