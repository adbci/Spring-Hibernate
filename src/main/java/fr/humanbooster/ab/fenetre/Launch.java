package fr.humanbooster.ab.fenetre;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Parse your nomProjet class package and create accordingly
 * every dao/ daoImpl and service/ serviceImpl file needed for Spring
 */
public class Launch {

    // define your working path using below template
    final static String path = "main\\java\\fr\\humanbooster\\ab\\fenetre";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Have you properly defined your working path using below template? (Y/N)");
        System.out.println(" -----> " + path);
        System.out.print("Your answer: ");

        String answer = scanner.nextLine();

        if (answer.equals("Y") || answer.equals("O") || answer.equals("y") || answer.equals("o")) {

            try {

                Dao daoUtils = new Dao();
                Business businessUtils = new Business();
                Service serviceUtils = new Service();

                List<String> dirs = new ArrayList();
                dirs.add("dao");
                dirs.add("service");

                for (String dir : dirs) {
                    createDir(dir);
                    createSubDir(dir);
                }

                createDir("controller");
                createTempFile("controller");
                createDir("tests");
                createTempFile("tests");
               // createDir("nomProjet");

//                workingDir = System.getProperty("user.dir");

                List<String> businessFiles = listFiles(System.getProperty("user.dir")+"\\src\\" + path +"\\business" );

                System.out.println(System.getProperty("user.dir")+"\\src\\" + path );
                System.out.println(businessFiles.toString());

                for (String fileName : businessFiles) {
                    // create nomProjet files
//                    writeFile(path, "nomProjet", fileName, businessUtils.createBusiness(path, fileName));
                    // create dao files
                    writeFile(path, "dao", fileName + "Dao", daoUtils.createDao(path, fileName));
                    writeFile(path, "dao\\daoImpl", fileName + "DaoImpl", daoUtils.createDaoImpl(path, fileName));
                    // create service files
                    writeFile(path, "service", fileName + "Service", serviceUtils.createService(path, fileName));
                    writeFile(path, "service\\serviceImpl", fileName + "ServiceImpl", serviceUtils.createServiceImpl(path, fileName));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            System.out.print("Please modify the path variable accordingly to your working path dir before going further.");
        }



        // below version creates nomProjet classes defined by a List you have to
        // provide manually

        /*
        try {

            Dao daoUtils = new Dao();
            business businessUtils = new business();
            Service serviceUtils = new Service();

            // create first dir tree
            createTree(path);

            List<String> dirs = new ArrayList();
            dirs.add("dao");
            dirs.add("service");

            for (String dir : dirs) {
                createDir(dir);
                createTempFile(dir);
                createSubDir(dir);
                createTempFile(dir + "\\" + dir + "Impl");
            }

            createDir("controller");
            createTempFile("controller");
            createDir("tests");
            createTempFile("tests");
            createDir("nomProjet");

            List<String> businessFiles = new ArrayList();
            businessFiles.add("Category");
            businessFiles.add("Player");

            for (String fileName : businessFiles) {
                // create nomProjet files
                writeFile(path, "nomProjet", fileName, businessUtils.createBusiness(path, fileName));
                // create dao files
                writeFile(path, "dao",fileName+"Dao", daoUtils.createDao(path, fileName));
                writeFile(path, "dao\\daoImpl",fileName+"DaoImpl", daoUtils.createDaoImpl(path, fileName));
                // create service files
                writeFile(path, "service",fileName+"Service", serviceUtils.createService(path, fileName));
                writeFile(path, "service\\serviceImpl",fileName+"ServiceImpl", serviceUtils.createServiceImpl(path, fileName));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    }


    /**
     * Create a dir tree
     *
     * @param arborescence name directory
     * @return true or false if success
     */
    static boolean createTree(String arborescence) {
        String currentDir = System.getProperty("user.dir") + "\\src\\";
        System.out.println(currentDir);

        File tree = new File(currentDir + arborescence);
        System.out.println(currentDir + arborescence);

        if (!tree.exists()) {
            if (tree.mkdirs()) {
                System.out.println("Succès! Arborescence " + arborescence + " créée!");
            } else {
                System.out.println("######### Echec! impossible de créer l'arborescence " + arborescence + "!");
                return false;
            }
        }
        return true;
    }

    /**
     * Create a directory
     *
     * @param dirName name directory
     * @return true or false if success
     */
    static boolean createDir(String dirName) {
        String currentDir = System.getProperty("user.dir") + "\\src\\" + path + "\\";
        System.out.println(currentDir);

        File dir = new File(currentDir + dirName);
        if (!dir.exists()) {
            if (dir.mkdir()) {
                System.out.println("Succès! Répertoire " + dirName + " créée!");
            } else {
                System.out.println("######### Echec! impossible de créer le répertoire " + dirName + "!");
                return false;
            }
        }
        return true;
    }


    /**
     * Create a tempfile inside current dir
     *
     * @param dirName name directory
     * @return true or false if success
     */
    static boolean createTempFile(String dirName) {
        String currentDir = System.getProperty("user.dir") + "\\src\\" + path + "\\" + dirName;
        System.out.println(currentDir);

        String fileName = "test.java";
        String subDir = dirName;

        File tempFile = new File(currentDir + "\\" + fileName);
        try {
            if (!tempFile.exists()) {
                if (tempFile.createNewFile()) {
                    System.out.println("Succès! Fichier temporaire " + fileName + " créée!");
                } else {
                    System.out.println("######## Echec! impossible de créer le fichier temporaire " + fileName + "!");
                    return false;
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return true;
    }


    /**
     * Create a subDirectory inside current one
     *
     * @param dirName name directory
     * @return true or false if success
     */
    static boolean createSubDir(String dirName) {
        String currentDir = System.getProperty("user.dir") + "\\src\\" + path + "\\" + dirName;
        System.out.println(currentDir);

        String subDir = dirName + "Impl";

        File dir = new File(currentDir + "\\" + subDir);
        if (!dir.exists()) {
            if (dir.mkdir()) {
                System.out.println("Succès! Sous-dossier " + subDir + " créée!");
            } else {
                System.out.println("######### Echec! impossible de créer le sous-dossier " + subDir + "!");
                return false;
            }
        }
        return true;
    }

    /**
     * Write a content inside a file
     *
     * @param path
     * @param dir
     * @param fileName
     * @param content
     * @return
     */
    static boolean writeFile(String path, String dir, String fileName, String content) {

        String currentDir = System.getProperty("user.dir") + "\\src\\" + path + "\\";
        System.out.println(currentDir);

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(currentDir + "\\" + dir + "\\" + fileName + ".java");
            bw = new BufferedWriter(fw);
            bw.write(content);

            System.out.println("Ecriture réussie!");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                System.out.println("## ERREUR impossible d'écrire le fichier " + fileName);
                ex.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Listing all files in a directory
     *
     * @param path
     */
    public static List<String> listFiles(String path) {
        File directory = new File(path);
        File[] fList = directory.listFiles();
        List<String> list = new ArrayList<>();
        for (File file : fList) {
            list.add(file.getName().replace(".java", ""));
        }
        return list;
    }

}
