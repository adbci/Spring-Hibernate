package fr.humanbooster.ab.fenetre;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit;
import java.util.EnumSet;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Date;

public class DateManager {

    public static java.sql.Date convertUtilToSql(java.util.Date dateUtil) {
        return new java.sql.Date(dateUtil.getTime());
    }

    public static java.util.Date convertSqlToUtil(java.sql.Date dateSql) {
        return new java.util.Date(dateSql.getTime());
    }

    public static String displayDateDiff(Date date1, Date date2) {
        Map<TimeUnit, Long> result = computeDiff(
                date1, date2
        );

        // System.out.println(result);
        Long jours = Long.valueOf(result.values().toArray()[0].toString());
        Long heures = Long.valueOf(result.values().toArray()[1].toString());
        Long minutes = Long.valueOf(result.values().toArray()[2].toString());
        Long secondes = Long.valueOf(result.values().toArray()[3].toString());

        List<String> donnees = new ArrayList<>();

        if (jours > 0) {
            if (jours > 1) {
                donnees.add(jours + " jours");
                System.out.println(jours + " jours");
            } else {
                donnees.add(jours + " jours");
                System.out.println(jours + " jour");
            }
        }
        if (heures > 0) {
            if (heures > 1) {
                donnees.add(heures + " heures");
                System.out.println(heures + " heures");
            } else {
                donnees.add(heures + " heure");
                System.out.println(heures + " heure");
            }
        }
        if (minutes > 0) {
            if (minutes > 1) {
                donnees.add(minutes + " minutes");
                System.out.println(minutes + " minutes");
            } else {
                donnees.add(minutes + " minute");
                System.out.println(minutes + " minute");
            }
        }
        if (secondes > 0) {
            if (secondes > 1) {
                donnees.add(secondes + " secondes");
                System.out.println(secondes + " secondes");
            } else {
                donnees.add(secondes + " seconde");
                System.out.println(secondes + " seconde");
            }
        }
        return donnees.toString();
    }

    private static Map<TimeUnit, Long> computeDiff(Date date1, Date date2) {
        long diffInMillies = date2.getTime() - date1.getTime();
        List<TimeUnit> units = new ArrayList<TimeUnit>(EnumSet.allOf(TimeUnit.class));
        Collections.reverse(units);

        Map<TimeUnit, Long> result = new LinkedHashMap<TimeUnit, Long>();
        long milliesRest = diffInMillies;
        for (TimeUnit unit : units) {
            long diff = unit.convert(milliesRest, TimeUnit.MILLISECONDS);
            long diffInMilliesForUnit = unit.toMillis(diff);
            milliesRest = milliesRest - diffInMilliesForUnit;
            result.put(unit, diff);
        }
        return result;
    }

}
