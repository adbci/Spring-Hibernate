<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Demande enregistr�e!</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
</head>
<body>

<div class="container-fluid">

    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center pt-20">
            <div class="btn-success">
                Demande enregistr�e!
            </div>
            <div class="pt-20">
                Merci ${demande.personne.civilite} ${demande.personne.nom} pour votre demande de devis!
                <br/>
                Elle a bien �t� enregistr�. Nos �quipes reviendront vers vous tr�s prochainement.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center pt-20">
        Vous pouvez d�sormais <a href="/logout">vous deconnecter</a>
            <br/>
            ou <a href="/demandePrix">effectuer une autre demande</a> de prix.
        </div>
    </div>
</div>
</body>
</html>